<?php


class Router
{

    /**
     * Router constructor.
     */

    protected $routes = [];

    public function define($routes)
    {
        $this->routes =$routes;
    }

    public function direct($uri)
    {
        //looking for wfflix.nl/teachers
        if (array_key_exists($uri, $this->routes)) {
            die(var_dump($this->routes));
            return $this->routes[$uri];
    }
        throw new Exception('No route defined');
    }
}
