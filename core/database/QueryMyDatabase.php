<?php


class QueryMyDatabase
{


    public $conn;

    public function __construct($conn)
    {
        $this->$conn;
    }

    public function selectAll()
    {
        $stmt = $this->conn->prepare('select first_name as Name, email from teachers');
        $stmt->execute();
        return $stmt->fetchAll();
    }
}