<?php


class connection
{
    public static function Make($config )
    {
        try {
            return new PDO (
                $config['connection'] . ';dbname=' . $config['dbname'],
                $config['username'],
                $config['password']
        );
        }    catch(PDOException $exception){
            die($exception->getMessage());
        }
    }
}