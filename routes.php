<?php

$router -> define ([
    '' => 'controllers/index.controller.php', // wfflix.nl/
    'teachers' => 'controllers/teachers.controller.php', // wfflix.nl/teachers
    'courses' => 'controllers/courses.controller.php', // wfflix.nl/courses
    'contact' => 'controllers/contact.controller.php' // wfflix.nl/contact
]);