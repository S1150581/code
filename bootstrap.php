<?php
require 'Router.php';
require 'core/database/connection.php';
require 'core/database/QueryMyDatabase.php';
$config = require 'config.php';

return new QueryMyDatabase(
    $conn = connection::make($config['database'])
);

$teachers = new QueryMyDatabase($conn);